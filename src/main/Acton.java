package main;

import main.ast.node.Program;
import main.compileError.CompileErrorException;
import main.visitor.nameAnalyser.NameAnalyser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import antlr.actonLexer;
import antlr.actonParser;
import main.visitor.codeGenerator.CodeGenerator;

import java.io.IOException;

//import main.visitor.astPrinter.ASTPrinter;

// Visit https://stackoverflow.com/questions/26451636/how-do-i-use-antlr-generated-parser-and-lexer
public class Acton {
    public static void main(String[] args) throws IOException {
        CharStream reader = CharStreams.fromFileName("./acton/" + args[0]);
        actonLexer lexer = new actonLexer(reader);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        actonParser parser = new actonParser(tokens);
        try{
            Program program = parser.program().p; // program is starting production rule
            NameAnalyser nameAnalyser = new NameAnalyser();
            CodeGenerator codeGenerator = new CodeGenerator();
            nameAnalyser.visit(program);
            if( nameAnalyser.numOfErrors() > 0 )
                throw new CompileErrorException();
            codeGenerator.visit(program);
        }
        catch(CompileErrorException compileError){
        }

    }
}