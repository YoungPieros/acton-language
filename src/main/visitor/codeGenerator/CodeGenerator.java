package main.visitor.codeGenerator;

//import javafx.beans.binding.BooleanExpression;
import main.ast.node.*;
import main.ast.node.Program;
import main.ast.node.declaration.*;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.declaration.handler.InitHandlerDeclaration;
import main.ast.node.declaration.handler.MsgHandlerDeclaration;
import main.ast.node.expression.operators.BinaryOperator;
import main.ast.node.expression.operators.UnaryOperator;
import main.ast.node.statement.*;
import main.ast.node.expression.*;
import main.ast.node.expression.values.*;
import main.ast.type.Type;
import main.ast.type.actorType.ActorType;
import main.ast.type.arrayType.ArrayType;
import main.ast.type.noType.NoType;
import main.ast.type.primitiveType.BooleanType;
import main.ast.type.primitiveType.IntType;
import main.ast.type.primitiveType.StringType;
import main.symbolTable.*;
import main.symbolTable.itemException.ItemNotFoundException;
import main.symbolTable.symbolTableVariableItem.SymbolTableLocalVariableItem;
import main.symbolTable.symbolTableVariableItem.SymbolTableVariableItem;
import main.visitor.VisitorImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class CodeGenerator extends VisitorImpl {

    int queueSize = 0;
    static String DIRECTORY = "./bin/";
    private Type lastExpressionType = new NoType();
    private String LIMIT_STACK_SIZE = "32";
    private String knownActorsTypes = "";
    private ArrayList <String> statementsCodes = new ArrayList<String>();
    private ArrayList <String> expressionCodes = new ArrayList<String>();
    private ArrayList <String> lastActorCodes;
    private ArrayList <String> defaultActorCodes;
    private ArrayList <String> msghandlerClassCodes = new ArrayList<String>();
    private ArrayList <String> mainCodes = new ArrayList<String>();

    private ArrayList <ArrayList <String>> msgHandlersInDefaultActor = new ArrayList< ArrayList<String>>();
    private SymbolTable actorSymbolTable;
    private SymbolTableHandlerItem msghandlerSymbolTable;

    private int currentLabel;
    private Stack <String> afterStack = new Stack<String>();
    private Stack <String> continueStack = new Stack<String>();
    private Stack <String> breakStack = new Stack<String>();
//    private String afterLabel = "";
//    private String breakLabel = "";
//    private String continueLabel = "";

    private boolean leftValueExpression = false;
    private boolean localVariable = false;
    private boolean arrayCall = false;
    private boolean lastActorVarAccess = false;

    ArrayList <String> actorByteCodes = new ArrayList<String>();

    private void resetActorCodes () {
        lastActorCodes = new ArrayList<String>();
        currentLabel = 0;
    }

    private String getNewLabel () {
        currentLabel++;
        return "Label" + Integer.toString(currentLabel);
    }

    private String changeString (String leftString, String rightString) {
        return leftString + rightString;
    }

    private void createMessageCodes () {
        ArrayList <String> MessageCodes;
    }

    private void createDefaultActorCodes () {
        defaultActorCodes = new ArrayList<String>();
        defaultActorCodes.add(".class public DefaultActor");
        defaultActorCodes.add(".super java/lang/Thread");
        defaultActorCodes.add("");
        defaultActorCodes.add(".method public <init>()V");
        defaultActorCodes.add(".limit stack 32");
        defaultActorCodes.add(".limit locals 1");
        defaultActorCodes.add("aload_0");
        defaultActorCodes.add("invokespecial java/lang/Thread/<init>()V");
        defaultActorCodes.add("return");
        defaultActorCodes.add(".end method");
        defaultActorCodes.add("");
    }

    private void finishDefaultActorCodes () {

    }

    private void createConstructor (ActorDeclaration actorDeclaration) {
        lastActorCodes.add(".method public <init>(I)V");
        lastActorCodes.add(".limit stack 32");
        lastActorCodes.add(".limit locals 2");
        lastActorCodes.add("aload_0");
        lastActorCodes.add("iload_1");
        lastActorCodes.add("invokespecial Actor/<init>(I)V");
        for (VarDeclaration actorVar : actorDeclaration.getActorVars())
            if (actorVar.getType() instanceof ArrayType) {
                lastActorCodes.add("aload_0");
                lastActorCodes.add("ldc " + ((ArrayType)actorVar.getType()).getSize());
                lastActorCodes.add("newarray int");
                lastActorCodes.add("putfield " + actorDeclaration.getName().getName() + "/" + actorVar.getIdentifier().getName() + " " + getTypeInJasmin(actorVar.getType()));
            }
            else if (actorVar.getType() instanceof StringType){
                lastActorCodes.add("aload_0");
                lastActorCodes.add("ldc \"\"");
                lastActorCodes.add("putfield " + actorDeclaration.getName().getName() + "/" + actorVar.getIdentifier().getName() + " " + getTypeInJasmin(actorVar.getType()));
            }
            else {
                lastActorCodes.add("aload_0");
                lastActorCodes.add("iconst_0");
                lastActorCodes.add("putfield " + actorDeclaration.getName().getName() + "/" + actorVar.getIdentifier().getName() + " " + getTypeInJasmin(actorVar.getType()));
            }
        lastActorCodes.add("return");
        lastActorCodes.add(".end method");
        lastActorCodes.add("");
    }

    private void createSetKnownActorFunction (ActorDeclaration actorDeclaration) {
        String methodArgs = "";
        ArrayList <VarDeclaration> knownActors = actorDeclaration.getKnownActors();
        for (VarDeclaration knownActor : knownActors)
            methodArgs = changeString(methodArgs, "L" + knownActor.getType() + ";");
        lastActorCodes.add(".method public setKnownActors(" + methodArgs + ")V");
        lastActorCodes.add(".limit stack " + LIMIT_STACK_SIZE);
        lastActorCodes.add(".limit locals " + Integer.toString(knownActors.size() + 1));
        for (int i = 0; i < knownActors.size(); i++) {
            lastActorCodes.add("aload_0");
            lastActorCodes.add("aload" + getLoadStoreCommand(i+1));
            lastActorCodes.add("putfield " + actorSymbolTable.getName() + "/" + knownActors.get(i).getIdentifier().getName() + " L" + knownActors.get(i).getType() + ";");
        }
        lastActorCodes.add("return");
        lastActorCodes.add(".end method");
        lastActorCodes.add("");
    }

    private void createActorDeclarationCodes (String actorName) {
        lastActorCodes = new ArrayList<String>();
        lastActorCodes.add(".class public " + actorName);
        lastActorCodes.add(".super Actor");
        lastActorCodes.add("");
    }

    private String getTypeInJasmin(Type type) {
        if (type instanceof IntType)
            return "I";
        if (type instanceof BooleanType)
            return "Z";
        if (type instanceof StringType)
            return "Ljava/lang/String;";
        if (type instanceof ArrayType)
            return "[I";
        if (type instanceof ActorType)
            return "L" + ((ActorType)type).getName().getName() + ";";
        return "";
    }

    private void saveByteCodesFile (ArrayList <String> codes, String filename) {
        File fileCreator = new File(CodeGenerator.DIRECTORY + filename + ".j");
        try {
            FileWriter fileWriter = new FileWriter(CodeGenerator.DIRECTORY + filename + ".j");
            for (String code : codes)
                fileWriter.write(code + "\n");
            fileWriter.close();
        }
        catch (IOException ioException) {
        }
    }

    private void createMsghandlerCodes (HandlerDeclaration handlerDeclaration) {
        createHandlerDeclaration (handlerDeclaration);
        createSendHandlerDeclaration (handlerDeclaration);
        addMsgHandlerToDefaultActor (handlerDeclaration);
        createMsgHandlerClass (handlerDeclaration);
    }

    private void createInitialHandlerCodes (HandlerDeclaration handlerDeclaration) {
        createHandlerDeclaration(handlerDeclaration);
    }

    private String getArgs (HandlerDeclaration handlerDeclaration) {
        String argsString = "";
        for (VarDeclaration arg : handlerDeclaration.getArgs())
            argsString = changeString(argsString, getTypeInJasmin(arg.getType()));
        return argsString;
    }

    private int getLocalSize (HandlerDeclaration handlerDeclaration) {
        return handlerDeclaration.getArgs().size() + handlerDeclaration.getLocalVars().size() + 2;
    }

    private void createHandlerDeclaration (HandlerDeclaration handlerDeclaration) {
        String argumentsString = getArgs(handlerDeclaration);
        msghandlerSymbolTable = ((SymbolTableHandlerItem)actorSymbolTable.getSymbolTableItems().get(SymbolTableHandlerItem.STARTKEY + handlerDeclaration.getName().getName()));
        if (handlerDeclaration instanceof MsgHandlerDeclaration) {
            argumentsString = changeString("LActor;", argumentsString);
        }
        lastActorCodes.add("");
        lastActorCodes.add("");
        lastActorCodes.add("");
        lastActorCodes.add(".method public " + handlerDeclaration.getName().getName() + "(" + argumentsString + ")V");
        lastActorCodes.add(".limit stack " + LIMIT_STACK_SIZE);
        lastActorCodes.add(".limit locals " + getLocalSize(handlerDeclaration));
        for (VarDeclaration localVar : handlerDeclaration.getLocalVars())
            if (localVar.getType() instanceof ArrayType) {
                lastActorCodes.add("ldc " + ((ArrayType)localVar.getType()).getSize());
                lastActorCodes.add("newarray int");
                lastActorCodes.add("astore" + getLocalVariableNumber(localVar.getIdentifier()));
            }
            else if (localVar.getType() instanceof StringType){
                lastActorCodes.add("ldc \"\"");
                lastActorCodes.add("astore" + getLocalVariableNumber(localVar.getIdentifier()));
            }
            else if (localVar.getType() instanceof IntType || localVar.getType() instanceof BooleanType) {
                lastActorCodes.add("iconst_0");
                lastActorCodes.add("istore" + getLocalVariableNumber(localVar.getIdentifier()));
            }
        for (Statement statement : handlerDeclaration.getBody()) {
//            afterStack.push(getNewLabel());
//            afterLabel = getNewLabel();
            visitStatement(statement);
//            afterStack.pop();
        }
        lastActorCodes.add("return");
        lastActorCodes.add(".end method");
        lastActorCodes.add("");
        lastActorCodes.add("");
        lastActorCodes.add("");
        lastActorCodes.add("");
    }

    private String getLoadCommand (VarDeclaration varDeclaration) {
        Type type = varDeclaration.getType();
        if (type instanceof IntType)
            return "iload";
        if (type instanceof BooleanType)
            return "iload";
        if (type instanceof StringType)
            return "aload";
        if (type instanceof ArrayType)
            return "aload";
        return "";
    }

    private void createSendHandlerDeclaration (HandlerDeclaration handlerDeclaration) {
        ArrayList <VarDeclaration> args = handlerDeclaration.getArgs();
        int localSize = args.size() + 2;
        String actorName = actorSymbolTable.getName();
        String functionName = msghandlerSymbolTable.getName();
        lastActorCodes.add(".method public send_" + handlerDeclaration.getName().getName() + "(LActor;" + getArgs(handlerDeclaration) + ")V");
        lastActorCodes.add(".limit stack " + LIMIT_STACK_SIZE);
        lastActorCodes.add(".limit locals " + localSize);
        lastActorCodes.add("aload_0");
        lastActorCodes.add("new " + actorName + "_" + functionName);
        lastActorCodes.add("dup");
        lastActorCodes.add("aload_0");
        lastActorCodes.add("aload_1");
        for (int i = 0; i < args.size(); i++)
            lastActorCodes.add(getLoadCommand (args.get(i)) + getLoadStoreCommand(i+2));
        lastActorCodes.add("invokespecial " + actorName + "_" + functionName + "/<init>(L" + actorName + ";LActor;" + getArgs(handlerDeclaration) + ")V");
        lastActorCodes.add("invokevirtual " + actorName + "/send(LMessage;)V");
        lastActorCodes.add("return");
        lastActorCodes.add(".end method");
        lastActorCodes.add("");
    }

    private ArrayList <String> getHashHandlerDeclaration (HandlerDeclaration handlerDeclaration) {
        ArrayList <String> hashString = new ArrayList<String>();
        hashString.add(handlerDeclaration.getName().getName());
        for (VarDeclaration arg : handlerDeclaration.getArgs())
            hashString.add(getTypeInJasmin(arg.getType()));
        return hashString;
    }

    private boolean isHandlerAvailableInDefaultActor (ArrayList <String> hashString) {
        for (ArrayList <String> handler : msgHandlersInDefaultActor)
            if (handler.equals(hashString))
                return true;
        return false;
    }

    private void addMsgHandlerToDefaultActor (HandlerDeclaration handlerDeclaration) {
        ArrayList <String> handlerHashMap = getHashHandlerDeclaration(handlerDeclaration);
        if (isHandlerAvailableInDefaultActor(handlerHashMap))
            return;
        msgHandlersInDefaultActor.add(handlerHashMap);
        String argsString = getArgs(handlerDeclaration);
        String functionName = handlerDeclaration.getName().getName();
        defaultActorCodes.add(".method public send_" + functionName + "(LActor;" + argsString + ")V");
        defaultActorCodes.add(".limit stack 32");
        defaultActorCodes.add(".limit locals " + (handlerDeclaration.getArgs().size() + 2));
        defaultActorCodes.add("getstatic java/lang/System/out Ljava/io/PrintStream;");
        defaultActorCodes.add("ldc \"there is no msghandler named " + functionName + " in sender\"");
        defaultActorCodes.add("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        defaultActorCodes.add("return");
        defaultActorCodes.add(".end method");
        defaultActorCodes.add("");
    }

    private void createMsghandlerClassConstructor (HandlerDeclaration handlerDeclaration) {
        ArrayList <VarDeclaration> handlerArguments = handlerDeclaration.getArgs();
        String actorName = actorSymbolTable.getName();
        String functionName = handlerDeclaration.getName().getName();
        String className = actorName + "_" + functionName;
        String args = "L" + actorName + ";LActor;" + getArgs(handlerDeclaration);
        msghandlerClassCodes.add(".method public <init>(" + args + ")V");
        msghandlerClassCodes.add(".limit stack 32");
        msghandlerClassCodes.add(".limit locals " + (handlerArguments.size() + 3));
        msghandlerClassCodes.add("aload_0");
        msghandlerClassCodes.add("invokespecial Message/<init>()V");
        msghandlerClassCodes.add("aload_0");
        msghandlerClassCodes.add("aload_1");
        msghandlerClassCodes.add("putfield " + className + "/receiver L" + actorName + ";");
        msghandlerClassCodes.add("aload_0");
        msghandlerClassCodes.add("aload_2");
        msghandlerClassCodes.add("putfield " + className + "/sender LActor;");
        for (int i = 0; i < handlerArguments.size(); i++) {
            Type type = handlerArguments.get(i).getType();
            msghandlerClassCodes.add("aload_0");
            if (type instanceof IntType || type instanceof BooleanType)
                msghandlerClassCodes.add("iload" + getLoadStoreCommand(i+3));
            else
                msghandlerClassCodes.add("aload" + getLoadStoreCommand(i+3));
            msghandlerClassCodes.add("putfield " + className + "/" + handlerArguments.get(i).getIdentifier().getName() + " " + getTypeInJasmin(handlerArguments.get(i).getType()));
        }
        msghandlerClassCodes.add("return");
        msghandlerClassCodes.add(".end method");
        msghandlerClassCodes.add("");
    }

    private void createMsghandlerClassExecute (HandlerDeclaration handlerDeclaration) {
        ArrayList <VarDeclaration> handlerArguments = handlerDeclaration.getArgs();
        String actorName = actorSymbolTable.getName();
        String functionName = handlerDeclaration.getName().getName();
        String className = actorName + "_" + functionName;
        String args = "L" + actorName + ";LActor;" + getArgs(handlerDeclaration);
        msghandlerClassCodes.add(".method public execute()V");
        msghandlerClassCodes.add(".limit stack 32");
        msghandlerClassCodes.add(".limit locals 1");
        msghandlerClassCodes.add("aload_0");
        msghandlerClassCodes.add("getfield " + className + "/receiver L" + actorName + ";");
        msghandlerClassCodes.add("aload_0");
        msghandlerClassCodes.add("getfield " + className + "/sender LActor;");
        for (VarDeclaration arg : handlerArguments) {
            msghandlerClassCodes.add("aload_0");
            msghandlerClassCodes.add("getfield " + className + "/" + arg.getIdentifier().getName() + " " + getTypeInJasmin(arg.getType()));
        }
        msghandlerClassCodes.add("invokevirtual " + actorName + "/" + functionName + "(LActor;" + getArgs(handlerDeclaration) + ")V");
        msghandlerClassCodes.add("return");
        msghandlerClassCodes.add(".end method");
        msghandlerClassCodes.add("");
    }

    private void createMsgHandlerClass (HandlerDeclaration handlerDeclaration) {
        ArrayList <VarDeclaration> handlerArguments = handlerDeclaration.getArgs();
        String actorName = actorSymbolTable.getName();
        String className = actorName + "_" + handlerDeclaration.getName().getName();
        msghandlerClassCodes = new ArrayList<String>();
        msghandlerClassCodes.add(".class public " + className);
        msghandlerClassCodes.add(".super Message");
        msghandlerClassCodes.add("");
        msghandlerClassCodes.add(".field private receiver L" + actorName + ";");
        msghandlerClassCodes.add(".field private sender LActor;");
        for (VarDeclaration arg : handlerArguments)
            msghandlerClassCodes.add(".field private " + arg.getIdentifier().getName() + " " + getTypeInJasmin(arg.getType()));
        msghandlerClassCodes.add("");
        createMsghandlerClassConstructor(handlerDeclaration);
        createMsghandlerClassExecute(handlerDeclaration);
        saveByteCodesFile(msghandlerClassCodes, className);
    }

    private void createMainClass () {
        mainCodes.add(".class public Main");
        mainCodes.add(".super java/lang/Object");
        mainCodes.add("");
        mainCodes.add(".method public <init>()V");
        mainCodes.add(".limit stack 32");
        mainCodes.add(".limit locals 1");
        mainCodes.add("0: aload_0");
        mainCodes.add("1: invokespecial java/lang/Object/<init>()V");
        mainCodes.add("4: return");
        mainCodes.add(".end method");
        mainCodes.add("");
    }

    private void createMainInstances (Main mainProgram) {
        int instanceCounter = 0;
        for (ActorInstantiation actorInstance: mainProgram.getMainActors()) {
            String type = ((ActorType)actorInstance.getType()).getName().getName();
            SymbolTableActorItem actorSymbol = (SymbolTableActorItem) SymbolTable.root.getSymbolTableItems().get(SymbolTableActorItem.STARTKEY + type);
            mainCodes.add("new " + type);
            mainCodes.add("dup");
            mainCodes.add("ldc " + actorSymbol.getActorDeclaration().getQueueSize());
            mainCodes.add("invokespecial " + type + "/<init>(I)V");
            mainCodes.add("astore" + getLoadStoreCommand(instanceCounter+1));
            instanceCounter++;
        }
    }

    private int getInstanceNumber (Main mainProgram, Identifier identifier) {
        ArrayList <ActorInstantiation> actorInstances = mainProgram.getMainActors();
        for (int i = 0; i < actorInstances.size(); i++)
            if (actorInstances.get(i).getIdentifier().getName().equals(identifier.getName())) {
                knownActorsTypes = changeString(knownActorsTypes, "L" + ((ActorType)actorInstances.get(i).getType()).getName().getName() + ";");
                return i + 1;
            }
        return 0;
    }

    private void createMainSetKnownActors (Main mainProgram) {
        int instanceCounter = 1;
        for (ActorInstantiation actorInstance : mainProgram.getMainActors()) {
            String type = ((ActorType)actorInstance.getType()).getName().getName();
            knownActorsTypes = "";
            mainCodes.add("aload" + getLoadStoreCommand(instanceCounter));
            for (Identifier identifier : actorInstance.getKnownActors())
                mainCodes.add("aload" + getLoadStoreCommand(getInstanceNumber(mainProgram, identifier)));
            mainCodes.add("invokevirtual " + type + "/setKnownActors(" + knownActorsTypes + ")V");
            instanceCounter++;
        }
    }

    private void createMainInitialFunctions (Main mainProgram) {
        int instanceCounter = 1;
        for (ActorInstantiation actorInstance : mainProgram.getMainActors()) {
            int argumentCounter = 0;
            String type = ((ActorType)actorInstance.getType()).getName().getName();
            ActorDeclaration actorDeclaration = ((SymbolTableActorItem)SymbolTable.root.getSymbolTableItems().get(SymbolTableActorItem.STARTKEY + type)).getActorDeclaration();
            InitHandlerDeclaration initialHandler = actorDeclaration.getInitHandler();
            if (initialHandler == null) {
                instanceCounter++;
                continue;
            }
            mainCodes.add("aload" + getLoadStoreCommand(instanceCounter));
            for (Expression expression : actorInstance.getInitArgs()) {
                expressionCodes = new ArrayList<String>();
                visitExpr(expression);
                mainCodes.addAll(expressionCodes);
            }
            mainCodes.add("invokevirtual " + type + "/initial(" + getArgs(initialHandler) + ")V");
            instanceCounter++;
        }
    }

    private void createMainStartFunctions (Main mainProgram) {
        int instanceCounter = 1;
        for (ActorInstantiation actorInstance : mainProgram.getMainActors()) {
            String type = ((ActorType)actorInstance.getType()).getName().getName();
            mainCodes.add("aload" + getLoadStoreCommand(instanceCounter));
            mainCodes.add("invokevirtual " + type + "/start()V");
            instanceCounter++;
        }
    }

    private void createMainFunction (Main mainProgram) {
        mainCodes.add(".method public static main([Ljava/lang/String;)V");
        mainCodes.add(".limit stack " + LIMIT_STACK_SIZE);
        mainCodes.add(".limit locals " + (mainProgram.getMainActors().size() + 1));
        createMainInstances (mainProgram);
        createMainSetKnownActors (mainProgram);
        createMainInitialFunctions (mainProgram);
        createMainStartFunctions (mainProgram);
        mainCodes.add("return");
        mainCodes.add(".end method");
        mainCodes.add("");
    }

    private void createEqualsExpression () {

        if (lastExpressionType instanceof StringType)
            expressionCodes.add("invokevirtual java/lang/String.equals(Ljava/lang/Object;)Z");
        else if (lastExpressionType instanceof IntType || lastExpressionType instanceof BooleanType) {
            String trueLabel = getNewLabel();
            String afterLabel = getNewLabel();
            expressionCodes.add("isub");
            expressionCodes.add("ifeq " + trueLabel);
            expressionCodes.add("iconst_0");
            expressionCodes.add("goto " + afterLabel);
            expressionCodes.add(trueLabel + ": ");
            expressionCodes.add("iconst_1");
            expressionCodes.add(afterLabel + ":");
        }
        else if (lastExpressionType instanceof ArrayType)
            expressionCodes.add("invokestatic java/util/Arrays.equals([I[I)Z");
        else if (lastExpressionType instanceof ActorType)
            expressionCodes.add("invokevirtual java/lang/Object.equals(Ljava/lang/Object;)Z");
    }

    public void createBranchCodes (String command, String trueLabel, String falseLabel) {
        if (lastExpressionType instanceof IntType || lastExpressionType instanceof BooleanType) {
            expressionCodes.add(command + " " + trueLabel);
            expressionCodes.add("goto " + falseLabel);
        }
        else {
            createEqualsExpression();
            expressionCodes.add("ifeq " + falseLabel);
            expressionCodes.add("goto " + trueLabel);
        }
    }

    private void branch(Expression condition, String nextTrueLabel, String nextFalseLabel) {
        if (condition instanceof UnaryExpression && (((UnaryExpression)condition).getUnaryOperator() == UnaryOperator.not))
            branch(((UnaryExpression) condition).getOperand(), nextFalseLabel, nextTrueLabel);
        else if (condition instanceof BinaryExpression) {
            if (((BinaryExpression) condition).getBinaryOperator() == BinaryOperator.eq) {
                visitLeftAndRightExpressions((BinaryExpression) condition);
//                createEqualsExpression ((BinaryExpression) condition);
                createBranchCodes("if_icmpeq", nextTrueLabel, nextFalseLabel);
            }
            else if (((BinaryExpression) condition).getBinaryOperator() == BinaryOperator.neq) {
                visitLeftAndRightExpressions((BinaryExpression) condition);
//                createEqualsExpression((BinaryExpression)condition);
                createBranchCodes("if_icmpeq", nextFalseLabel, nextTrueLabel);
            }
            else if (((BinaryExpression) condition).getBinaryOperator() == BinaryOperator.lt) {
                visitLeftAndRightExpressions((BinaryExpression) condition);
                createBranchCodes("if_icmpge", nextFalseLabel, nextTrueLabel);
            }
            else if (((BinaryExpression) condition).getBinaryOperator() == BinaryOperator.gt) {
                visitLeftAndRightExpressions((BinaryExpression) condition);
                createBranchCodes("if_icmple", nextFalseLabel, nextTrueLabel);
            }
            else if (((BinaryExpression) condition).getBinaryOperator() == BinaryOperator.and) {
                String middleLabel = getNewLabel();
                branch(((BinaryExpression) condition).getLeft(), middleLabel, nextFalseLabel);
                expressionCodes.add(middleLabel + ":");
                branch(((BinaryExpression) condition).getRight(), nextTrueLabel, nextFalseLabel);
            }
            else if (((BinaryExpression) condition).getBinaryOperator() == BinaryOperator.or) {
                String middleLabel = getNewLabel();
                branch(((BinaryExpression) condition).getLeft(), nextTrueLabel, middleLabel);
                expressionCodes.add(middleLabel + ":");
                branch(((BinaryExpression) condition).getRight(), nextTrueLabel, nextFalseLabel);
            }
        }
        else if (condition instanceof Identifier || condition instanceof ActorVarAccess) {
            visitExpr(condition);
            expressionCodes.add("ifeq " + nextFalseLabel);
            expressionCodes.add("goto " + nextTrueLabel);
        }
        else if (condition instanceof BooleanValue) {
            if (((BooleanValue) condition).getConstant())
                expressionCodes.add("goto " + nextTrueLabel);
            else
                expressionCodes.add("goto " + nextFalseLabel);
        }
        lastExpressionType = new BooleanType();
    }

    private String getLocalVariableNumber (Identifier identifier) {
        HandlerDeclaration handlerDeclaration = ((SymbolTableHandlerItem)msghandlerSymbolTable).getHandlerDeclaration();
        ArrayList <VarDeclaration> args = handlerDeclaration.getArgs();
        ArrayList <VarDeclaration> localVars = handlerDeclaration.getLocalVars();
        for (int i = 0; i < args.size(); i++)
            if (identifier.getName().equals(args.get(i).getIdentifier().getName()))
                if (handlerDeclaration instanceof InitHandlerDeclaration)
                    return getLoadStoreCommand(i+1);
                else
                    return getLoadStoreCommand(i+2);
        for (int i = 0; i < localVars.size(); i++)
            if (identifier.getName().equals(localVars.get(i).getIdentifier().getName()))
                if (handlerDeclaration instanceof InitHandlerDeclaration)
                    return getLoadStoreCommand(args.size() + 1);
                else
                    return getLoadStoreCommand(i + args.size() + 2);
        return "";
    }

    private void setExpressionCodes (ArrayList <String> actorByteCodes) {
        lastActorCodes.addAll(expressionCodes);
        expressionCodes = new ArrayList<String>();
    }

    private String getLoadStoreCommand (int number) {
        return ((number < 4) ? "_" : " ") + number;
    }

    void createActorFields (VarDeclaration varDeclaration) {
        if (varDeclaration.getType() instanceof ActorType)
            lastActorCodes.add(".field " + varDeclaration.getIdentifier().getName() + " L" + varDeclaration.getType() + ";");
        else
            lastActorCodes.add(".field " + varDeclaration.getIdentifier().getName() + " " + getTypeInJasmin(varDeclaration.getType()));
    }

    void visitLeftAndRightExpressions (BinaryExpression binaryExpression) {
        visitExpr(binaryExpression.getLeft());
        visitExpr(binaryExpression.getRight());
    }

    void createTrueAndAfterCommandWithIF_ICMP (String command) {
        String trueLabel = getNewLabel();
        String afterLabel = getNewLabel();
        expressionCodes.add(command + " " + trueLabel);
        expressionCodes.add("iconst_0");
        expressionCodes.add("goto " + afterLabel);
        expressionCodes.add(trueLabel + ": ");
        expressionCodes.add("iconst_1");
        expressionCodes.add(afterLabel + ": ");

    }

    @Override
    public void visit (Program program) {
        createMessageCodes();
        createDefaultActorCodes();
        for (ActorDeclaration actorDeclaration : program.getActors())
            actorDeclaration.accept(this);
        saveByteCodesFile(defaultActorCodes, "DefaultActor");
        program.getMain().accept(this);
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        actorSymbolTable = ((SymbolTableActorItem)SymbolTable.root.getSymbolTableItems().get(SymbolTableActorItem.STARTKEY + actorDeclaration.getName().getName())).getActorSymbolTable();
        resetActorCodes();
        createActorDeclarationCodes (actorDeclaration.getName().getName());
        for (VarDeclaration knownActor : actorDeclaration.getKnownActors())
            createActorFields(knownActor);
        for (VarDeclaration actorVar : actorDeclaration.getActorVars())
            createActorFields(actorVar);

        lastActorCodes.add("");
        createConstructor(actorDeclaration);
        createSetKnownActorFunction(actorDeclaration);
        if (actorDeclaration.getInitHandler() != null)
            actorDeclaration.getInitHandler().accept(this);
        for (MsgHandlerDeclaration msgHandler : actorDeclaration.getMsgHandlers())
            msgHandler.accept(this);
        saveByteCodesFile(lastActorCodes, actorDeclaration.getName().getName());
    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        if (handlerDeclaration instanceof InitHandlerDeclaration)
            createInitialHandlerCodes (handlerDeclaration);
        else
            createMsghandlerCodes (handlerDeclaration);
    }

    @Override
    public void visit(VarDeclaration varDeclaration) {
    }

    @Override
    public void visit(Main programMain) {
        createMainClass();
        createMainFunction(programMain);
        saveByteCodesFile(mainCodes, "Main");
    }

    @Override
    public void visit(ActorInstantiation actorInstantiation) {
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        if (unaryExpression.getUnaryOperator() == UnaryOperator.not) {
            expressionCodes.add("iconst_1");
            visitExpr(unaryExpression.getOperand());
            setExpressionCodes(lastActorCodes);
            expressionCodes.add("isub");
            lastExpressionType = new BooleanType();
        }
        else if (unaryExpression.getUnaryOperator() == UnaryOperator.minus) {
            visitExpr(unaryExpression.getOperand());
            setExpressionCodes(lastActorCodes);
            expressionCodes.add("ineg");
            lastExpressionType = new IntType();
        }
        else if (unaryExpression.getUnaryOperator() == UnaryOperator.preinc) {
            Expression lvalue = unaryExpression.getOperand();
            Expression plusExpr = new BinaryExpression(lvalue, new IntValue(1, new IntType()), BinaryOperator.add);
            Assign preIncStatement = new Assign(lvalue, plusExpr);
            visitStatement(preIncStatement);
            visitExpr(unaryExpression.getOperand());
            setExpressionCodes(lastActorCodes);
            lastExpressionType = new IntType();
        }
        else if (unaryExpression.getUnaryOperator() == UnaryOperator.postinc) {
            visitExpr(unaryExpression.getOperand());
            setExpressionCodes(lastActorCodes);
            Expression lvalue = unaryExpression.getOperand();
            Expression plusExpr = new BinaryExpression(lvalue, new IntValue(1, new IntType()), BinaryOperator.add);
            Assign preIncStatement = new Assign(lvalue, plusExpr);
            visitStatement(preIncStatement);
            lastExpressionType = new IntType();
        }
        else if (unaryExpression.getUnaryOperator() == UnaryOperator.predec) {
            Expression lvalue = unaryExpression.getOperand();
            Expression plusExpr = new BinaryExpression(lvalue, new IntValue(1, new IntType()), BinaryOperator.sub);
            Assign preIncStatement = new Assign(lvalue, plusExpr);
            visitStatement(preIncStatement);
            visitExpr(unaryExpression.getOperand());
            setExpressionCodes(lastActorCodes);
            lastExpressionType = new IntType();
        }
        else if (unaryExpression.getUnaryOperator() == UnaryOperator.postdec) {
            visitExpr(unaryExpression.getOperand());
            setExpressionCodes(lastActorCodes);
            Expression lvalue = unaryExpression.getOperand();
            Expression plusExpr = new BinaryExpression(lvalue, new IntValue(1, new IntType()), BinaryOperator.sub);
            Assign preIncStatement = new Assign(lvalue, plusExpr);
            visitStatement(preIncStatement);
            lastExpressionType = new IntType();
        }
    }

    private void printArray (ArrayList <String> array) {
        for (String s : array)
            System.out.println(s);
    }
    @Override
    public void visit(BinaryExpression binaryExpression) {

        if (binaryExpression.getBinaryOperator() == BinaryOperator.assign) {
            setExpressionCodes(lastActorCodes);
            Assign assignStatement = new Assign(binaryExpression.getLeft(), binaryExpression.getRight());
            visitStatement(assignStatement);
            visitExpr(binaryExpression.getLeft());
        }
        else if (binaryExpression.getBinaryOperator() == BinaryOperator.eq) {
            visitLeftAndRightExpressions(binaryExpression);
            createEqualsExpression ();
            lastExpressionType = new BooleanType();
        }
        else if (binaryExpression.getBinaryOperator() == BinaryOperator.neq) {
            visitLeftAndRightExpressions(binaryExpression);
            createEqualsExpression ();
            expressionCodes.add("iconst_1");
            expressionCodes.add("ixor");
            lastExpressionType = new BooleanType();
        }
        else if (binaryExpression.getBinaryOperator() == BinaryOperator.add) {
            visitLeftAndRightExpressions(binaryExpression);
            expressionCodes.add("iadd");
            lastExpressionType = new IntType();
        }
        else if (binaryExpression.getBinaryOperator() == BinaryOperator.sub) {
            visitLeftAndRightExpressions(binaryExpression);
            expressionCodes.add("isub");
            lastExpressionType = new IntType();
        }
        else if (binaryExpression.getBinaryOperator() == BinaryOperator.div) {
            visitLeftAndRightExpressions(binaryExpression);
            expressionCodes.add("idiv");
            lastExpressionType = new IntType();
        }
        else if (binaryExpression.getBinaryOperator() == BinaryOperator.mod) {
            visitLeftAndRightExpressions(binaryExpression);
            expressionCodes.add("irem");
            lastExpressionType = new IntType();
        }
        else if (binaryExpression.getBinaryOperator() == BinaryOperator.mult) {
            visitLeftAndRightExpressions(binaryExpression);
            expressionCodes.add("imul");
            lastExpressionType = new IntType();
        }
        else {
            if (binaryExpression.getBinaryOperator() == BinaryOperator.lt) {
                visitLeftAndRightExpressions(binaryExpression);
                createTrueAndAfterCommandWithIF_ICMP ("if_icmplt");
            }
            else if (binaryExpression.getBinaryOperator() == BinaryOperator.gt) {
                visitLeftAndRightExpressions(binaryExpression);
                createTrueAndAfterCommandWithIF_ICMP("if_icmpgt");
            }
            else if (binaryExpression.getBinaryOperator() == BinaryOperator.and) {
                visitLeftAndRightExpressions(binaryExpression);
                expressionCodes.add("iand");
            }
            else if (binaryExpression.getBinaryOperator() == BinaryOperator.or) {
                visitLeftAndRightExpressions(binaryExpression);
                expressionCodes.add("ior");
            }
            lastExpressionType = new BooleanType();
        }
    }

    @Override
    public void visit(ArrayCall arrayCall) {
        boolean savedLeftValue   = leftValueExpression;
        this.arrayCall = true;
        visitExpr(arrayCall.getArrayInstance());
        this.arrayCall = false;
        leftValueExpression = false;
        visitExpr(arrayCall.getIndex());
        leftValueExpression = savedLeftValue;
        if (!leftValueExpression)
            expressionCodes.add("iaload");
        lastExpressionType = new IntType();
    }

    @Override
    public void visit(ActorVarAccess actorVarAccess) {
        lastActorVarAccess = true;
        visitExpr(actorVarAccess.getVariable());
        lastActorVarAccess = false;
    }

    @Override
    public void visit(Identifier identifier) {
        if (!lastActorVarAccess && (!leftValueExpression || this.arrayCall)) {
            try {
                SymbolTableVariableItem item = (SymbolTableVariableItem) msghandlerSymbolTable.getHandlerSymbolTable().getInCurrentScope(SymbolTableLocalVariableItem.STARTKEY + identifier.getName());
                expressionCodes.add(getLoadCommand(item.getVarDeclaration()) + getLocalVariableNumber(identifier));
                lastExpressionType = item.getType();
            } catch (ItemNotFoundException notFoundItem) {
                lastExpressionType = ((SymbolTableVariableItem) actorSymbolTable.getSymbolTableItems().get(SymbolTableVariableItem.STARTKEY + identifier.getName())).getType();
                expressionCodes.add("aload_0");
                expressionCodes.add("getfield " + actorSymbolTable.getName() + "/" + identifier.getName() + " " + getTypeInJasmin(lastExpressionType));
            }
        } else {
            try {
                if (lastActorVarAccess) {
                    SymbolTableVariableItem item = (SymbolTableVariableItem) actorSymbolTable.getSymbolTableItems().get(SymbolTableVariableItem.STARTKEY + identifier.getName());
                    expressionCodes.add("aload_0");
                    localVariable = false;
                    lastExpressionType = item.getType();
                    if (!leftValueExpression)
                        expressionCodes.add("getfield " + actorSymbolTable.getName() + "/" + identifier.getName() + " " + getTypeInJasmin(lastExpressionType));
                    return;
                }
                SymbolTableVariableItem item = (SymbolTableVariableItem) msghandlerSymbolTable.getHandlerSymbolTable().getInCurrentScope(SymbolTableVariableItem.STARTKEY + identifier.getName());
                lastExpressionType = item.getType();
                localVariable = true;
            } catch (ItemNotFoundException notFoundItem) {
                SymbolTableVariableItem item = (SymbolTableVariableItem) actorSymbolTable.getSymbolTableItems().get(SymbolTableVariableItem.STARTKEY + identifier.getName());
                lastExpressionType = item.getType();
                expressionCodes.add("aload_0");
                if (this.arrayCall)
                    expressionCodes.add("getfield " + actorSymbolTable.getName() + "/" + identifier.getName() + " " + getTypeInJasmin(item.getType()));
                localVariable = false;
            }
        }

    }

    @Override
    public void visit(Self self) {
        expressionCodes.add("aload_0");
        lastExpressionType = new ActorType(new Identifier(actorSymbolTable.getName()));
    }

    @Override
    public void visit(Sender sender) {
        expressionCodes.add("aload_1");
        lastExpressionType = new ActorType(new Identifier("Actor"));
    }

    @Override
    public void visit(BooleanValue value) {
        if (value.getConstant())
            expressionCodes.add("iconst_1");
        else
            expressionCodes.add("iconst_0");
        lastExpressionType = new BooleanType();
    }

    @Override
    public void visit(IntValue value) {
        if (value.getConstant() <= 4)
            expressionCodes.add("iconst_" + value.getConstant());
        else
            expressionCodes.add("ldc " + value.getConstant());
        lastExpressionType = new IntType();
    }

    @Override
    public void visit(StringValue value) {
        expressionCodes.add("ldc " + value.getConstant());
        lastExpressionType = new StringType();
    }

    @Override
    public void visit(MsgHandlerCall msgHandlerCall) {
        visitExpr(msgHandlerCall.getInstance());
        setExpressionCodes(lastActorCodes);
        String callerType = ((ActorType)lastExpressionType).getName().getName();
        String functionCall = "send_" +  msgHandlerCall.getMsgHandlerName().getName();
        ArrayList <Expression> args = msgHandlerCall.getArgs();
        String argTypes = "";
        lastActorCodes.add("aload_0");
        for (Expression arg : args) {
            visitExpr(arg);
            argTypes = changeString(argTypes, getTypeInJasmin(lastExpressionType));
            setExpressionCodes(lastActorCodes);
        }
        lastActorCodes.add("invokevirtual " + callerType + "/" + functionCall + "(LActor;" + argTypes + ")V");
    }

    @Override
    public void visit(Block block) {
        for (Statement statement : block.getStatements())
            statement.accept(this);
    }

    @Override
    public void visit(Conditional conditional) {
        String thenLabel = getNewLabel();
        afterStack.push(getNewLabel());
        if (conditional.getElseBody() == null) {
            branch(conditional.getExpression(), thenLabel, afterStack.lastElement());
            setExpressionCodes(lastActorCodes);
            lastActorCodes.add(thenLabel + ": ");
            visitStatement(conditional.getThenBody());
        }
        else {
            String elseLabel = getNewLabel();
            branch(conditional.getExpression(), thenLabel, elseLabel);
            setExpressionCodes(lastActorCodes);
            lastActorCodes.add(thenLabel + ": ");
            visitStatement(conditional.getThenBody());
            lastActorCodes.add("goto " + afterStack.lastElement());
            lastActorCodes.add(elseLabel + ": ");
            visitStatement(conditional.getElseBody());
        }
        lastActorCodes.add(afterStack.pop() + ": ");
    }

    @Override
    public void visit(For loop) {
        visitStatement(loop.getInitialize());
        afterStack.push(getNewLabel());
        breakStack.push(afterStack.lastElement());
        continueStack.push(getNewLabel());
        String bodyLabel = getNewLabel();
        String testLabel = (loop.getCondition() == null) ? bodyLabel : getNewLabel();
        if (loop.getCondition() != null) {
            lastActorCodes.add(testLabel + ": ");
            branch(loop.getCondition(), bodyLabel, afterStack.lastElement());
            setExpressionCodes(lastActorCodes);
        }
        lastActorCodes.add(bodyLabel + ": ");
        visitStatement(loop.getBody());
        lastActorCodes.add(continueStack.lastElement() + ": ");
        visitStatement(loop.getUpdate());
        lastActorCodes.add("goto " + testLabel);
        lastActorCodes.add(afterStack.pop() + ": ");
        continueStack.pop();
        breakStack.pop();
    }

    @Override
    public void visit(Break b) {
        lastActorCodes.add("goto " + breakStack.lastElement());
    }

    @Override
    public void visit(Continue c) {
        lastActorCodes.add("goto " + continueStack.lastElement());
    }

    @Override
    public void visit(Print print) {
        lastActorCodes.add("getstatic java/lang/System.out Ljava/io/PrintStream;");
        visitExpr(print.getArg());
        setExpressionCodes(lastActorCodes);
        if (lastExpressionType instanceof ArrayType) {
            lastActorCodes.add("invokestatic java/util/Arrays.toString([I)Ljava/lang/String;");
            lastActorCodes.add("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        }
        else
            lastActorCodes.add("invokevirtual java/io/PrintStream/println(" + getTypeInJasmin(lastExpressionType) + ")V");
    }

    @Override
    public void visit(Assign assign) {
        Type savedType = lastExpressionType;
        Type lvtype;
        boolean savedLocalVariable;
        leftValueExpression = true;
        visitExpr(assign.getlValue());
        setExpressionCodes (lastActorCodes);
        lvtype = lastExpressionType;
        savedLocalVariable = localVariable;
        leftValueExpression = false;
        if (lastExpressionType instanceof BooleanType) {
            String thenLabel = getNewLabel();
            String elseLabel = getNewLabel();
            String afterLabel = getNewLabel();
            branch(assign.getrValue(), thenLabel, elseLabel);
            setExpressionCodes(lastActorCodes);
            lastActorCodes.add(thenLabel + ": ");
            lastActorCodes.add("iconst_1");
            lastActorCodes.add("goto " + afterLabel);
            lastActorCodes.add(elseLabel + ": ");
            lastActorCodes.add("iconst_0");
            lastActorCodes.add(afterLabel + ": ");
        }
        else {
            visitExpr(assign.getrValue());
            setExpressionCodes(lastActorCodes);
        }
        if (assign.getlValue() instanceof ArrayCall)
            lastActorCodes.add("iastore");
        else if (assign.getlValue() instanceof ActorVarAccess || !savedLocalVariable) {
            if (assign.getlValue() instanceof ActorVarAccess)
                lastActorCodes.add("putfield " + actorSymbolTable.getName() + "/" + ((ActorVarAccess) assign.getlValue()).getVariable().getName() + " " + getTypeInJasmin(lastExpressionType));
            else if (assign.getlValue() instanceof Identifier)
                lastActorCodes.add("putfield " + actorSymbolTable.getName() + "/" + ((Identifier)assign.getlValue()).getName() +  " " + getTypeInJasmin(lastExpressionType));
        }
        else {
            if (lastExpressionType instanceof IntType || lastExpressionType instanceof BooleanType)
                lastActorCodes.add("istore" + getLocalVariableNumber((Identifier)(assign.getlValue())));
            else
                lastActorCodes.add("astore" + getLocalVariableNumber((Identifier)assign.getlValue()));
        }
        lastExpressionType = savedType;
    }
}
