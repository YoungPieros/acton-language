package main.visitor.nameAnalyser;

import main.ast.node.*;
import main.ast.node.Program;
import main.ast.node.declaration.*;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.declaration.handler.MsgHandlerDeclaration;
import main.ast.node.expression.operators.BinaryOperator;
import main.ast.node.expression.operators.UnaryOperator;
import main.ast.node.statement.*;
import main.ast.node.expression.*;
import main.ast.node.expression.values.*;
import main.ast.type.Type;
import main.ast.type.actorType.ActorType;
import main.ast.type.arrayType.ArrayType;
import main.ast.type.noType.NoType;
import main.ast.type.primitiveType.BooleanType;
import main.ast.type.primitiveType.IntType;
import main.ast.type.primitiveType.StringType;
import main.symbolTable.*;
import main.symbolTable.symbolTableVariableItem.*;
import main.symbolTable.itemException.*;
import main.visitor.VisitorImpl;

import java.awt.image.BaseMultiResolutionImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class NameAnalyser extends VisitorImpl {
    private SymbolTableConstructor symbolTableConstructor;
    private TraverseState traverseState;
    private SymbolTableActorParentLinker symbolTableActorLinker;
    private ArrayList<String> errors;
    private int lastIndexOfVariable;
    private Type lastType;
    private int loopNumber;
    private boolean isHandlerInitializer;
    private boolean checkIdentifier;
    private boolean isLastTypeLeftValue;
    private SymbolTable symbolTable;
    private SymbolTableMainItem mainSymbolItem;
    private boolean mainScopeInTypeError;

    private void AddError (int lineNumber, String message) {
        errors.add("Line:" + Integer.toString(lineNumber) + ":" + message);
    }

    public NameAnalyser()
    {
        symbolTableConstructor = new SymbolTableConstructor();
        symbolTableActorLinker = new SymbolTableActorParentLinker();
        errors = new ArrayList<>();
        lastIndexOfVariable = 0;
        setState(TraverseState.symbolTableConstruction);
        lastType = new NoType();
        loopNumber = 0;
        isHandlerInitializer = false;
        checkIdentifier = false;
        symbolTable = new SymbolTable();
        isLastTypeLeftValue = false;
        mainSymbolItem = null;
        mainScopeInTypeError = false;
    }

    public int numOfErrors()
    {
        return errors.size();
    }

    private void switchState()
    {
        if(traverseState == TraverseState.symbolTableConstruction)
            setState(TraverseState.nameError);
        else if(traverseState == TraverseState.nameError && errors.size() > 0)
            setState(TraverseState.PrintError);
//        else if (traverseState == TraverseState.nameError)
//            setState(TraverseState.typeError);
        else if (traverseState == TraverseState.typeError)
            setState(TraverseState.PrintError);
        else
            setState(TraverseState.Exit);
    }

    private void setState(TraverseState traverseState)
    {
        this.traverseState = traverseState;
    }

    private SymbolTableHandlerItem checkDeclarationMsgHandlerInActor (String actorName, Identifier msgHandlerName) {
        String keyValue = SymbolTableActorItem.STARTKEY + actorName;
        SymbolTableActorItem item = null;
        while (true) {
            try {
                item = (SymbolTableActorItem) SymbolTable.root.getInCurrentScope(keyValue);
                SymbolTableHandlerItem handlerItem = (SymbolTableHandlerItem) (item.getActorSymbolTable().getInCurrentScope(SymbolTableHandlerItem.STARTKEY + msgHandlerName.getName()));
                return handlerItem;
            }
            catch (ItemNotFoundException itemNotFound) {
                if (item.getParentName() == null)
                    return null;
                keyValue = SymbolTableActorItem.STARTKEY + item.getParentName();
            }
        }
    }

    private SymbolTableActorItem getActorSymbolTable (String actorName) {
        try {
            if (SymbolTable.root.getInCurrentScope(SymbolTableActorItem.STARTKEY + actorName) != null)
                return (SymbolTableActorItem) SymbolTable.root.getInCurrentScope(SymbolTableActorItem.STARTKEY + actorName);
        }
        catch (ItemNotFoundException notFoundItem) {
            return null;
        }
        return null;
    }

    private boolean isActorSubType (String actorType, String baseActorType) {
        if (actorType.equals(baseActorType))
            return true;
        String currentType = actorType;
        while (true) {
            try {
                currentType = ((SymbolTableActorItem)SymbolTable.root.getInCurrentScope(SymbolTableActorItem.STARTKEY + currentType)).getParentName();
                if (currentType == null)
                    return false;
                if (currentType.equals(baseActorType))
                    return true;
            }
            catch (ItemNotFoundException notFoundItem) {
                return false;
            }
        }
    }

    private void setHandlerDeclarationToSymbolTable (HandlerDeclaration handlerDeclaration) {
        String name = SymbolTableHandlerItem.STARTKEY + handlerDeclaration.getName().getName();
        try {
            SymbolTableHandlerItem handlerItem = (SymbolTableHandlerItem) SymbolTable.top.getInCurrentScope(name);
            symbolTable = handlerItem.getHandlerSymbolTable();
            symbolTable.setPreSymbolTable(SymbolTable.top);
        }
        catch (ItemNotFoundException itemNotFound) {
        }
    }

    private void addActorDeclarationToSymbolTable (ActorDeclaration actorDeclaration) {
        String name = actorDeclaration.getName().getName();
        try {
            SymbolTableActorItem actorItem = (SymbolTableActorItem) SymbolTable.root.getInCurrentScope(SymbolTableActorItem.STARTKEY + name);
            SymbolTable next = actorItem.getActorSymbolTable();
            SymbolTable.push(next);
        }
        catch (ItemNotFoundException itemNotFound) {
        }
    }

    private void checkForPropertyRedefinition(ActorDeclaration actorDeclaration)
    {
        String name = actorDeclaration.getName().getName();
        if(name.indexOf('$') != -1)
            errors.add("Line:" + actorDeclaration.getName().getLine() + ":Redefinition of actor " + name.substring(name.indexOf('$') + 1));
        try
        {
            SymbolTableActorItem actorItem = (SymbolTableActorItem) SymbolTable.root.getInCurrentScope(SymbolTableActorItem.STARTKEY + name);
            SymbolTable next = actorItem.getActorSymbolTable();
            SymbolTable.push(next);
        }
        catch(ItemNotFoundException itemNotFound)
        {
            System.out.println("there is an error in pushing class symbol table");
        }
    }

    private void checkForPropertyRedefinitionInParentScopes(VarDeclaration varDeclaration)
    {
        String name = varDeclaration.getIdentifier().getName();
        Set<SymbolTable> visitedSymbolTables = new HashSet<>();
        String variableKey = SymbolTableVariableItem.STARTKEY + name;
        SymbolTable current = SymbolTable.top.getPreSymbolTable();
        visitedSymbolTables.add(SymbolTable.top);
        while(current != null &&  !visitedSymbolTables.contains(current))
        {
            visitedSymbolTables.add(current);
            try {
                current.getInCurrentScope(variableKey);
                SymbolTableVariableItem variable = (SymbolTableVariableItem) SymbolTable.top.getInCurrentScope(variableKey);
                errors.add("Line:" + varDeclaration.getIdentifier().getLine() + ":Redefinition of variable " + name.substring(name.indexOf('$') + 1));
                return;
            }
            catch(ItemNotFoundException itemNotFound)
            {
                current = current.getPreSymbolTable();
            }
        }
    }

    private void checkForPropertyRedefinition(VarDeclaration varDeclaration)
    {
        String name = varDeclaration.getIdentifier().getName();
        if(name.indexOf('$') != -1) {
            errors.add("Line:" + varDeclaration.getIdentifier().getLine() + ":Redefinition of variable " + name.substring(name.indexOf('$') + 1));
            return;
        }
        try {
            SymbolTableVariableItem varItem = (SymbolTableVariableItem) SymbolTable.top.getInCurrentScope(SymbolTableVariableItem.STARTKEY + name);
            varItem.setIndex(lastIndexOfVariable++);
            SymbolTable.top.updateInCurrentScope(SymbolTableVariableItem.STARTKEY + name , varItem);
            if(varItem instanceof SymbolTableActorVariableItem || varItem instanceof SymbolTableKnownActorItem)
                checkForPropertyRedefinitionInParentScopes(varDeclaration);
        }
        catch(ItemNotFoundException itemNotFound)
        {
            System.out.println("an error occurred");
        }
    }

    private Type checkForNotDeclaringInParentScopes (String variableName, int lineNumber) {
        Set<SymbolTable> visitedSymbolTables = new HashSet<>();
        String variableKey = SymbolTableVariableItem.STARTKEY + variableName;
        SymbolTable current = SymbolTable.top;
        while(current != null &&  !visitedSymbolTables.contains(current))
        {
            visitedSymbolTables.add(current);
            try {
                SymbolTableVariableItem variable = (SymbolTableVariableItem)current.getInCurrentScope(variableKey);
                lastType = variable.getType();
                return lastType;
            }
            catch(ItemNotFoundException itemNotFound)
            {
                current = current.getPreSymbolTable();
            }
        }
        lastType = new NoType();
        AddError(lineNumber, "variable " + variableName + " is not declared");
        return lastType;
    }

    private void checkForNotDeclaring (String actorDeclarationName, String keyValue, int lineNumber) {
    }

    private Type checkForNotDeclaring(String variableName, int lineNumber)
    {
        try {
            SymbolTableVariableItem symbolItem = (SymbolTableVariableItem) symbolTable.getInCurrentScope(SymbolTableVariableItem.STARTKEY + variableName);
            lastType = symbolItem.getType();
            return lastType;
        }
        catch (ItemNotFoundException itemNotFound) {
            return checkForNotDeclaringInParentScopes (variableName, lineNumber);
        }
    }

    private void checkForPropertyRedefinitionInParentScope(MsgHandlerDeclaration msgHandlerDeclaration)
    {
        String name = msgHandlerDeclaration.getName().getName();
        String methodKey = SymbolTableHandlerItem.STARTKEY + name;
        SymbolTable current = SymbolTable.top.getPreSymbolTable();
        Set<SymbolTable> visitedSymbolTables = new HashSet<>();
        visitedSymbolTables.add(SymbolTable.top);
        while(current != null && !visitedSymbolTables.contains(current))
        {
            visitedSymbolTables.add(current);
            try {
                current.getInCurrentScope(methodKey);
                SymbolTableHandlerItem method = (SymbolTableHandlerItem) SymbolTable.top.getInCurrentScope(methodKey);
//                method.setName("$" + method.getName());
//                SymbolTable.top.updateInCurrentScope(methodKey , method);
                errors.add("Line:" + msgHandlerDeclaration.getName().getLine() + ":Redefinition of msghandler " + name.substring(name.indexOf('$') + 1));
                return;
            }
            catch(ItemNotFoundException itemNotFound)
            {
                current = current.getPreSymbolTable();
            }
        }
    }

    private void checkForPropertyRedefinition(HandlerDeclaration handlerDeclaration)
    {
        String name = handlerDeclaration.getName().getName();
        SymbolTable next;
        String methodKey = SymbolTableHandlerItem.STARTKEY + name;
        try
        {
            next = ((SymbolTableHandlerItem)SymbolTable.top.getInCurrentScope(methodKey)).getHandlerSymbolTable();
        }
        catch(ItemNotFoundException itemNotFound)
        {
            System.out.println("an error occurred in pushing method symbol table " + handlerDeclaration.getName().getName());
            return;
        }
        if(name.indexOf('$') != -1) {
            errors.add("Line:" + handlerDeclaration.getName().getLine() + ":Redefinition of msghandler " + name.substring(name.indexOf('$') + 1));
            SymbolTable.push(next);
            return;
        }
        if(handlerDeclaration instanceof MsgHandlerDeclaration)
            checkForPropertyRedefinitionInParentScope((MsgHandlerDeclaration) handlerDeclaration);

        SymbolTable.push(next);
    }

    private void pushMainSymbolTable(){
        try{
            SymbolTableMainItem mainItem = (SymbolTableMainItem) SymbolTable.root.getInCurrentScope(SymbolTableMainItem.STARTKEY + "main");
            SymbolTable next = mainItem.getMainSymbolTable();
            SymbolTable.push(next);
        }
        catch(ItemNotFoundException itemNotFound)
        {
            System.out.println("there is an error in pushing class symbol table");
        }
    }

    @Override
    public void visit(Program program){

        while(traverseState != TraverseState.Exit) {
            if (traverseState == TraverseState.symbolTableConstruction)
                symbolTableConstructor.constructProgramSymbolTable();
            else if (traverseState == TraverseState.nameError)
                symbolTableActorLinker.findActorsParents(program);
            else if(traverseState == TraverseState.PrintError) {
                for (String error : errors)
                    System.out.println(error);
                return;
            }

            for(ActorDeclaration actorDeclaration : program.getActors())
                actorDeclaration.accept(this);
            program.getMain().accept(this);
            switchState();
        }
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        if (traverseState == TraverseState.symbolTableConstruction)
            symbolTableConstructor.construct(actorDeclaration);
        else if (traverseState == TraverseState.nameError) {
            checkForPropertyRedefinition(actorDeclaration);
            //TODO: check cyclic inheritance
            if(symbolTableActorLinker.hasCyclicInheritance(actorDeclaration)){
                errors.add("Line:" + actorDeclaration.getLine() + ":Cyclic inheritance involving actor " + actorDeclaration.getName().getName());
            }
            if (actorDeclaration.getQueueSize() <= 0) {
                errors.add("Line:" + actorDeclaration.getLine() + ":Queue size must be positive");
            }
        }
        else if (traverseState == TraverseState.typeError) {
            try {
                if (actorDeclaration.getParentName() != null)
                    SymbolTable.root.getInCurrentScope(SymbolTableActorItem.STARTKEY + actorDeclaration.getParentName().getName());
            }
            catch (ItemNotFoundException exception) {
                AddError(actorDeclaration.getParentName().getLine(), "actor " + actorDeclaration.getParentName().getName() + " is not declared");
            }
            addActorDeclarationToSymbolTable (actorDeclaration);
            for (VarDeclaration knownActor : actorDeclaration.getKnownActors()) {
                try {
                    SymbolTable.root.getInCurrentScope(SymbolTableActorItem.STARTKEY + ((ActorType) knownActor.getType()).getName().getName());
                }
                catch (ItemNotFoundException notFoundItem) {
                    AddError(knownActor.getIdentifier().getLine(), "actor " + ((ActorType) knownActor.getType()).getName().getName() + " is not declared");
                }
            }

        }

        visitExpr(actorDeclaration.getName());
        visitExpr(actorDeclaration.getParentName());
        for(VarDeclaration varDeclaration: actorDeclaration.getKnownActors())
            varDeclaration.accept(this);
        for(VarDeclaration varDeclaration: actorDeclaration.getActorVars())
            varDeclaration.accept(this);
        if(actorDeclaration.getInitHandler() != null)
            actorDeclaration.getInitHandler().accept(this);
        for(MsgHandlerDeclaration msgHandlerDeclaration: actorDeclaration.getMsgHandlers())
            msgHandlerDeclaration.accept(this);
        SymbolTable.pop();
    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        if(handlerDeclaration == null)
            return;
        isHandlerInitializer = handlerDeclaration.getName().getName().equals("initial");
        if (traverseState == TraverseState.symbolTableConstruction)
            symbolTableConstructor.construct(handlerDeclaration);
        else if (traverseState == TraverseState.nameError)
            checkForPropertyRedefinition(handlerDeclaration);
        else if (traverseState == TraverseState.typeError)
            setHandlerDeclarationToSymbolTable(handlerDeclaration);

        visitExpr(handlerDeclaration.getName());
        for(VarDeclaration argDeclaration: handlerDeclaration.getArgs())
            argDeclaration.accept(this);
        for(VarDeclaration localVariable: handlerDeclaration.getLocalVars())
            localVariable.accept(this);
        if (traverseState == TraverseState.typeError)
            checkIdentifier = true;
        for(Statement statement : handlerDeclaration.getBody())
            visitStatement(statement);
        if (traverseState != TraverseState.typeError)
            SymbolTable.pop();
        checkIdentifier = false;
        isHandlerInitializer = false;
    }

    @Override
    public void visit(VarDeclaration varDeclaration) {
        if(varDeclaration == null)
            return;
        if(traverseState == TraverseState.nameError) {
            checkForPropertyRedefinition(varDeclaration);
            if(varDeclaration.getType() instanceof ArrayType){
                if (((ArrayType)varDeclaration.getType()).getSize() <= 0){
                    errors.add("Line:" + varDeclaration.getType().getLine() + ":Array size must be positive");
                }
            }
        }
        visitExpr(varDeclaration.getIdentifier());
    }

    @Override
    public void visit(Main programMain) {
        if(programMain == null)
            return;
        if (traverseState == TraverseState.symbolTableConstruction)
            symbolTableConstructor.construct(programMain);
        else if (traverseState == TraverseState.nameError)
            pushMainSymbolTable();
        else if (traverseState == TraverseState.typeError) {
            mainSymbolItem = (SymbolTableMainItem) SymbolTable.root.getSymbolTableItems().get(SymbolTableMainItem.STARTKEY + "main");
            mainScopeInTypeError = true;
        }
        for(ActorInstantiation mainActor : programMain.getMainActors())
            mainActor.accept(this);
        SymbolTable.pop();
    }

    @Override
    public void visit(ActorInstantiation actorInstantiation) {
        if(actorInstantiation == null)
            return;

        if (traverseState == TraverseState.nameError)
            checkForPropertyRedefinition(actorInstantiation);

        if (traverseState == TraverseState.typeError) {
            SymbolTableActorItem actorSymbol = getActorSymbolTable(((ActorType)actorInstantiation.getType()).getName().getName());
            if (actorSymbol == null) {
                AddError(actorInstantiation.getLine(), "actor " + ((ActorType) actorInstantiation.getType()).getName().getName() + " is not declared");
                for (Identifier knownActor : actorInstantiation.getKnownActors()) {
                    try {
                        mainSymbolItem.getMainSymbolTable().getInCurrentScope(SymbolTableVariableItem.STARTKEY + knownActor.getName());
                    }
                    catch (ItemNotFoundException notFoundItem) {
                        AddError(actorInstantiation.getLine(), "variable " + knownActor.getName() + " is not declared");
                    }
                }
                for (Expression initArg : actorInstantiation.getInitArgs())
                    visitExpr(initArg);
            }
            else {
                boolean errorNotMatch = false;
                int counter = -1;
                ArrayList <VarDeclaration> knownActors = actorSymbol.getActorDeclaration().getKnownActors();
                ArrayList <VarDeclaration> initArgs = new ArrayList<VarDeclaration>();
                if (actorSymbol.getActorDeclaration().getInitHandler() != null)
                    initArgs = actorSymbol.getActorDeclaration().getInitHandler().getArgs();
                if (actorInstantiation.getKnownActors().size() != actorSymbol.getActorDeclaration().getKnownActors().size())
                    errorNotMatch = true;
                for (Identifier knownActor : actorInstantiation.getKnownActors()) {
                    try {
                        counter++;
                        SymbolTableVariableItem symbolTableItem = (SymbolTableVariableItem) mainSymbolItem.getMainSymbolTable().getInCurrentScope(SymbolTableVariableItem.STARTKEY + knownActor.getName());
                        if (!errorNotMatch && !isActorSubType(((ActorType)(symbolTableItem.getType())).getName().getName(), ((ActorType)knownActors.get(counter).getType()).getName().getName()))
                            errorNotMatch = true;
                    }
                    catch (ItemNotFoundException itemNotFound) {
                        AddError(knownActor.getLine(), "variable " + knownActor.getName() + " is not declared");
                    }
                }
                if (errorNotMatch)
                    AddError(actorInstantiation.getLine(), "knownactors do not match with definition");
                counter = -1;
                errorNotMatch = false;
                if (actorInstantiation.getInitArgs().size() != initArgs.size())
                    errorNotMatch = true;
                for (Expression initArg : actorInstantiation.getInitArgs()) {
                    counter++;
                    checkIdentifier = true;
                    visitExpr(initArg);
                    if (!errorNotMatch && !(lastType instanceof NoType) && (lastType.getClass() != initArgs.get(counter).getType().getClass() ||
                            (lastType instanceof ActorType && initArgs.get(counter).getType() instanceof ActorType && ((ArrayType)lastType).getSize() != ((ArrayType)initArgs.get(counter).getType()).getSize())))
                        errorNotMatch = true;
                    checkIdentifier = false;
                }
                if (errorNotMatch)
                    AddError(actorInstantiation.getLine(), "arguments do not match with definition");
            }
        }
        else {
            visitExpr(actorInstantiation.getIdentifier());
            for (Identifier knownActor : actorInstantiation.getKnownActors())
                visitExpr(knownActor);
            for (Expression initArg : actorInstantiation.getInitArgs())
                visitExpr(initArg);
        }
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        UnaryOperator uo = unaryExpression.getUnaryOperator();
        if(unaryExpression == null)
            return;
        if (traverseState == TraverseState.typeError) {
            checkIdentifier = true;
            lastType = new NoType();
        }
        visitExpr(unaryExpression.getOperand());
        if (traverseState == TraverseState.typeError) {
            if (!(lastType instanceof NoType)) {
                if (uo == UnaryOperator.not && !(lastType instanceof BooleanType)) {
                    AddError(unaryExpression.getLine(), "unsupported operand type for " + uo.name());
                    lastType = new NoType();
                }
                else if (uo == UnaryOperator.minus && !(lastType instanceof IntType)) {
                    AddError(unaryExpression.getLine(), "unsupported operand type for " + uo.name());
                    lastType = new NoType();
                }
                else if ((uo == UnaryOperator.postinc || uo == UnaryOperator.preinc)) {
                    if (!(lastType instanceof IntType))
                        AddError(unaryExpression.getLine(), "unsupported operand type for increment");
                    if (!isLastTypeLeftValue)
                        AddError(unaryExpression.getLine(), "lvalue required as increment operand");
                    if (!isLastTypeLeftValue || !(lastType instanceof IntType))
                        lastType = new NoType();
                }
                else if ((uo == UnaryOperator.postdec || uo == UnaryOperator.predec)) {
                    if (!(lastType instanceof IntType))
                        AddError(unaryExpression.getLine(), "unsupported operand type for decrement");
                    if (!isLastTypeLeftValue)
                        AddError(unaryExpression.getLine(), "lvalue required as decrement operand");
                    if (!isLastTypeLeftValue || !(lastType instanceof IntType))
                        lastType = new NoType();
                }
            }
            checkIdentifier = false;
            isLastTypeLeftValue = false;
        }
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        Type leftType = new NoType();
        Type rightType = new NoType();
        if (binaryExpression == null)
            return;
        if (traverseState == TraverseState.typeError) {
            checkIdentifier = true;
            isLastTypeLeftValue = false;
            lastType = new NoType();
        }
        visitExpr(binaryExpression.getLeft());
        if (traverseState == TraverseState.typeError) {
            isLastTypeLeftValue = false;
            checkIdentifier = true;
            leftType = lastType;
        }
        visitExpr(binaryExpression.getRight());
        if (traverseState == TraverseState.typeError) {

            rightType = lastType;
            BinaryOperator bo = binaryExpression.getBinaryOperator();
            if (!(leftType instanceof NoType) || !(rightType instanceof NoType)) {
                if (bo == BinaryOperator.eq ) {
                    if ((!(leftType instanceof NoType) && !(rightType instanceof NoType)) && (leftType.getClass() != rightType.getClass())) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else if ((leftType instanceof ArrayType && rightType instanceof ArrayType)) {
                        if (((ArrayType) leftType).getSize() != ((ArrayType) rightType).getSize()) {
                            AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                            lastType = new NoType();
                        }
                        else
                            lastType = new BooleanType();
                    }
                    else
                        lastType = new BooleanType();
                }
                else if (bo == BinaryOperator.neq) {
                    if ((!(leftType instanceof NoType) && !(rightType instanceof NoType)) && (leftType.getClass() != rightType.getClass())) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else if (leftType instanceof ArrayType && rightType instanceof ArrayType) {
                        if (((ArrayType) leftType).getSize() != ((ArrayType) rightType).getSize()) {
                            AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                            lastType = new BooleanType();
                        }
                    }
                    else
                        lastType = new BooleanType();
                }
                else if (bo == BinaryOperator.gt) {
                    if ((!(leftType instanceof IntType) && !(leftType instanceof NoType)) || (!(rightType instanceof IntType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new BooleanType();
                }
                else if (bo == BinaryOperator.lt) {
                    if ((!(leftType instanceof IntType) && !(leftType instanceof NoType)) || (!(rightType instanceof IntType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new BooleanType();
                }
                else if (bo == BinaryOperator.add) {
                    if ((!(leftType instanceof IntType) && !(leftType instanceof NoType)) || (!(rightType instanceof IntType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new IntType();
                }
                else if (bo == BinaryOperator.sub) {
                    if ((!(leftType instanceof IntType) && !(leftType instanceof NoType)) || (!(rightType instanceof IntType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new IntType();
                }
                else if (bo == BinaryOperator.mult) {
                    if ((!(leftType instanceof IntType) && !(leftType instanceof NoType)) || (!(rightType instanceof IntType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new IntType();
                }
                else if (bo == BinaryOperator.div) {
                    if ((!(leftType instanceof IntType) && !(leftType instanceof NoType)) || (!(rightType instanceof IntType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new IntType();
                }
                else if (bo == BinaryOperator.mod) {
                    if ((!(leftType instanceof IntType) && !(leftType instanceof NoType)) || (!(rightType instanceof IntType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new IntType();
                }
                else if (bo == BinaryOperator.and) {
                    if ((!(leftType instanceof BooleanType) && !(leftType instanceof NoType)) || (!(rightType instanceof BooleanType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new BooleanType();
                }
                else if (bo == BinaryOperator.or) {
                    if ((!(leftType instanceof BooleanType) && !(leftType instanceof NoType)) || (!(rightType instanceof BooleanType) && !(rightType instanceof NoType))) {
                        AddError(binaryExpression.getLine(), "unsupported operand type for " + bo.name());
                        lastType = new NoType();
                    }
                    else
                        lastType = new BooleanType();
                }
                if ((leftType instanceof NoType) || (rightType instanceof NoType))
                    lastType = new NoType();
            }
            else
                lastType = new NoType();
            isLastTypeLeftValue = false;
            checkIdentifier = false;
        }
    }

    @Override
    public void visit(ArrayCall arrayCall) {
        Type saveLastType = new NoType();
        if (traverseState == TraverseState.typeError)
            checkIdentifier = true;
        visitExpr(arrayCall.getArrayInstance());
        if (traverseState == TraverseState.typeError && !(lastType instanceof NoType) && !(lastType instanceof ArrayType)) {
            if (!(arrayCall.getArrayInstance() instanceof ActorVarAccess)) {
                AddError(arrayCall.getArrayInstance().getLine(), "unsupported operand type for []");
                saveLastType = new NoType();
            }
        }
        else if (traverseState == TraverseState.typeError) {
            saveLastType = lastType;
            checkIdentifier = true;
        }
        visitExpr(arrayCall.getIndex());
        if (traverseState == TraverseState.typeError && !(lastType instanceof NoType) && !(lastType instanceof IntType)) {
            AddError(arrayCall.getIndex().getLine(), "int type is expected to calling array");
        }
        lastType = (saveLastType instanceof NoType) ? saveLastType : new IntType();
        isLastTypeLeftValue = true;
    }

    @Override
    public void visit(ActorVarAccess actorVarAccess) {
        if(actorVarAccess == null)
            return;
        if (traverseState == TraverseState.typeError) {
            if (mainScopeInTypeError) {
                AddError(actorVarAccess.getLine(), "self doesn't refer to any actor");
                checkIdentifier = false;
            }
            else {
                lastType = new ActorType(new Identifier(SymbolTable.top.getName()));
                checkIdentifier = true;
            }
        }
        visitExpr(actorVarAccess.getSelf());
        visitExpr(actorVarAccess.getVariable());
    }

    @Override
    public void visit(Identifier identifier) {
        if(identifier == null)
            return;
        if (traverseState == TraverseState.typeError && checkIdentifier) {
            if (!mainScopeInTypeError)
                checkForNotDeclaring(identifier.getName(), identifier.getLine());
            else {
                try {
                    SymbolTableVariableItem symbolItem = (SymbolTableVariableItem) mainSymbolItem.getMainSymbolTable().getInCurrentScope(SymbolTableVariableItem.STARTKEY + identifier.getName());
                    lastType = symbolItem.getType();
                }
                catch (ItemNotFoundException notFoundItem) {
                    AddError(identifier.getLine(), "variable " + identifier.getName() + " is not declared");
                    lastType = new NoType();
                }
            }
            isLastTypeLeftValue = true;
            checkIdentifier = false;
        }
    }

    @Override
    public void visit(Self self) {
        if (traverseState == TraverseState.typeError) {
            lastType = new ActorType(new Identifier(SymbolTable.top.getName()));
            isLastTypeLeftValue = false;
        }
    }

    @Override
    public void visit(Sender sender) {
        if (traverseState == TraverseState.typeError) {
            if (isHandlerInitializer)
                AddError(sender.getLine(), "no sender in initial msghandler");
            if (mainScopeInTypeError)
                AddError(sender.getLine(), "sender doesn't refer to any actor");
            lastType = new ActorType(new Identifier("Sender"));
            isLastTypeLeftValue = false;
        }
    }

    @Override
    public void visit(BooleanValue value) {
        if (traverseState == TraverseState.typeError) {
            lastType = new BooleanType();
            isLastTypeLeftValue = false;
        }
    }

    @Override
    public void visit(IntValue value) {
        if (traverseState == TraverseState.typeError) {
            lastType = new IntType();
            isLastTypeLeftValue = false;
        }
    }

    @Override
    public void visit(StringValue value) {
        if (traverseState == TraverseState.typeError) {
            lastType = new StringType();
            isLastTypeLeftValue = false;
        }    }

    @Override
    public void visit(MsgHandlerCall msgHandlerCall) {
        Type type = new NoType();
        SymbolTableHandlerItem symbolTableHandlerItem = null;
        Expression caller = msgHandlerCall.getInstance();
        String callerName = "";
        if (caller instanceof Identifier)
            callerName = ((Identifier) caller).getName();
        if(msgHandlerCall == null) {
            return;
        }
        try {
            visitExpr(msgHandlerCall.getInstance());
            ArrayList<Type> argTypes = new ArrayList<Type>();
            if (traverseState == TraverseState.typeError ) {
                if (!(caller instanceof Sender)) {
                    if (caller instanceof Self)
                        type = new ActorType(new Identifier(SymbolTable.top.getName()));
                    else
                        type = checkForNotDeclaring(callerName, msgHandlerCall.getMsgHandlerName().getLine());
                    if (!(type instanceof NoType) && !(type instanceof ActorType))
                        AddError(msgHandlerCall.getInstance().getLine(), "variable " + ((Identifier)msgHandlerCall.getInstance()).getName() + " is not callable");
                    if (!(type instanceof NoType) && (type instanceof ActorType)) {
                        symbolTableHandlerItem = checkDeclarationMsgHandlerInActor(type.toString(), msgHandlerCall.getMsgHandlerName());
                        if (symbolTableHandlerItem == null)
                            AddError(msgHandlerCall.getLine(), "there is no msghandler named " + msgHandlerCall.getMsgHandlerName().getName() + " in actor " + type.toString());
                        else {
                            argTypes = symbolTableHandlerItem.getArgTypes();
                            if (argTypes.size() != msgHandlerCall.getArgs().size()) {
                                AddError(msgHandlerCall.getMsgHandlerName().getLine(), "arguments do not match with definition");
                                for (Expression argument : msgHandlerCall.getArgs())
                                    visitExpr(argument);
                            }
                            else {
                                int counter = 0;
                                boolean notMatchError = false;
                                for (Expression argument : msgHandlerCall.getArgs()) {
                                    checkIdentifier = true;
                                    visitExpr(argument);
                                    if (!notMatchError && !(lastType instanceof NoType) && (lastType.getClass() != argTypes.get(counter).getClass())) {
                                        AddError(argument.getLine(), "arguments do not match with definition");
                                        notMatchError = true;
                                    }
                                    else if (lastType instanceof ArrayType && argTypes.get(counter) instanceof ArrayType && ((ArrayType) lastType).getSize() != ((ArrayType) argTypes.get(counter)).getSize()) {
                                        AddError(argument.getLine(), "arguments do not match with definition");
                                        notMatchError = true;
                                    }
                                    counter++;
                                }
                            }
                        }
                    }
                }
                if (caller instanceof Sender) {
                    for (Expression argument : msgHandlerCall.getArgs()) {
                        checkIdentifier = true;
                        visitExpr(argument);
                    }
                }
            }
            else {
                visitExpr(msgHandlerCall.getMsgHandlerName());
                for (Expression argument : msgHandlerCall.getArgs())
                    visitExpr(argument);
            }
        }
        catch(NullPointerException npe) {
            System.out.println("null pointer exception occurred");
        }
    }

    @Override
    public void visit(Block block) {
        if(block == null)
            return;
        for(Statement statement : block.getStatements())
            visitStatement(statement);
    }

    @Override
    public void visit(Conditional conditional) {
        if (traverseState == TraverseState.typeError) {
            lastType = new NoType();
            checkIdentifier = true;
        }
        visitExpr(conditional.getExpression());
        if (traverseState == TraverseState.typeError)
            if (!(lastType instanceof NoType) && !(lastType instanceof BooleanType))
                AddError(conditional.getExpression().getLine(), "condition type must be Boolean");
        visitStatement(conditional.getThenBody());
        visitStatement(conditional.getElseBody());
    }

    @Override
    public void visit(For loop) {
        loopNumber++;
        visitStatement(loop.getInitialize());
        visitExpr(loop.getCondition());
        if (traverseState == TraverseState.typeError)
            if (loop.getCondition() != null && !(lastType instanceof NoType) && !(lastType instanceof BooleanType)) {
                AddError(loop.getLine(), "condition type must be Boolean");
            }
        visitStatement(loop.getUpdate());
        visitStatement(loop.getBody());
        loopNumber--;
    }

    @Override
    public void visit(Break b) {
        if (traverseState == TraverseState.typeError && loopNumber == 0)
            AddError(b.getLine(), "break statement not within loop");
    }

    @Override
    public void visit(Continue c) {
        if (traverseState == TraverseState.typeError && loopNumber == 0)
            AddError(c.getLine(), "continue statement not within loop");
    }

    @Override
    public void visit(Print print) {
        if(print == null)
            return;
        if (traverseState == TraverseState.typeError)
            checkIdentifier = true;
        visitExpr(print.getArg());
        if (traverseState == TraverseState.typeError) {
            if (!(lastType instanceof NoType || lastType instanceof BooleanType || lastType instanceof StringType || lastType instanceof IntType || lastType instanceof ArrayType))
                AddError(print.getArg().getLine(), "unsupported type for print");
            isLastTypeLeftValue = false;
        }
    }

    @Override
    public void visit(Assign assign) {
        Type leftType = null;
        boolean localLeftValue = false;
        if (traverseState == TraverseState.typeError) {
            checkIdentifier = true;
            lastType = new NoType();
        }
        visitExpr(assign.getlValue());
        if (traverseState == TraverseState.typeError) {
            leftType = lastType;
            localLeftValue = isLastTypeLeftValue;
            checkIdentifier = true;
        }
        visitExpr(assign.getrValue());
        if (traverseState == TraverseState.typeError) {
            if (!localLeftValue)
                AddError(assign.getlValue().getLine(), "left side of assignment must be a valid lvalue");
            if (!(lastType instanceof NoType) && !(leftType instanceof NoType) && (leftType.getClass() != lastType.getClass()))
                AddError(assign.getLine(), "unsupported operand type for =");
            else if (!(lastType instanceof NoType) && !(leftType instanceof NoType) && leftType instanceof ActorType && lastType instanceof ActorType) {
                if (isActorSubType(((ActorType) leftType).getName().getName(), ((ActorType) lastType).getName().getName()))
                    AddError(assign.getLine(), "unsupported operand type for =");
            }
            else if ((lastType instanceof ArrayType) && (leftType instanceof ArrayType))
                if (((ArrayType) lastType).getSize() != ((ArrayType) leftType).getSize())
                    AddError(assign.getLine(), "operation assign requires equal array sizes");
            else
                checkIdentifier = false;
            isLastTypeLeftValue = false;
        }
    }
}
