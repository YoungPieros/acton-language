package main.visitor.nameAnalyser;

public enum TraverseState {
    symbolTableConstruction, nameError, typeError, PrintError , Exit
}
