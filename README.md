# ACTON LANGUAGE

#### You Can Read Document Of This Language In Document Folder <br>
#### There Are Some Acton Examples file In Examples Folder

<br />

## `essential java libraries:`
1- antlr.jar <br>
2- jasmin.jar

<br />
<br />
<br />
<br />

## `preparing:`
##### add antlr jar file to project libraries (this jar file is available in antlr folder)
##### comment line 3 in Acton.java if your java sdk version is later jdk 11.*
##### copy contents of base needed files in 'base' folder to bin folder.

<br />

## `write acton program:`
##### write your Acton program in put its file in 'acton' folder (you should use .act format, and name the file like example.act)

<br />

## `compile codes:`
##### compile Acton.java and run output compiler with the name of your acton file (like example.act)
##### now the outputs of compiled classes are in bin folder in *.j styles
##### open terminal or command prompt in bin and write: java -jar jasmin.jar *jar

<br />

## `run output program:`
##### now run: java Main to running your program :)

# <br /><br /><br />